#!/bin/sh
#Script removes all ofed packages

dpkg -r libdapl-dev libdapl2 dapl2-utils ibsim-utils libumad2sim0 \
ibutils libibdm1 libibdm-dev infiniband-diags libcxgb3-1 libcxgb3-dev \
libcxgb3-1-dbg libibcm-dev libibcm1 libibcommon-dev libibcommon1 \
libibmad-dev libibmad1 libibumad-dev libibumad1 libibverbs1 \
libibverbs-dev ibverbs-utils libipathverbs1 libipathverbs-dev \
libipathverbs1-dbg libmlx4-1 libmlx4-dev libmlx4-1-dbg \
libmthca1 libmthca-dev libmthca1-dbg libnes-dev libnes0 \
librdmacm1 librdmacm-dev librdmacm1-dbg rdmacm-utils \
libsdp1 mstflint ofa-kernel-source ofed-docs ofed \
opensm opensm-doc libopensm2 libopensm2-dev perftest \
qlvnictools qperf rds-tools sdpnetstat srptools tvflash \
mpitests libopenmpi1
